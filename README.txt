
Profile Tools for Drupal 6.x
----------------------------
Profile Tools module allows to improve Drupal core's Profile module usability
for end-users.

1. Set default profile tab on user's account edit page
  This feature allows you easily set your custom profile category as a default
  tab on the user's edit profile page.

2. Move User Picture Upload widget to your own profile tab
  This feature allows you easily move user picture upload interface to your
  custom profile tab.

Please, propose your features to extend this module!

Requirements
------------
Profile tools requires Drupal core's Profile module to be installed.

Configuration
-------------
1. Default profile tab.
  1. Create your custom profile category.
  2. Go to the Profile settings page <admin/user/profile>
  3. Click on the `Default Tab` tab link and select your default category.
  
2. Moving User Picture Upload widget.
  1. Go to the Users settings page <admin/user/settings>
  2. Enable picture support if not enabled before.
  3. Select your category to place widget on it and adjust it's weight.

Installation
------------
Profile tools can be installed like any other Drupal module -- place it in the
modules directory for your site and enable it on the `admin/build/modules` page.

Maintainers
-----------
- Dmitry Danilevsky (abarmot) - dmitry@danilevsky.com 
