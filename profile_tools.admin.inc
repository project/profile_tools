<?php

/**
 * @file
 * Settings function for Profile Tools module.
 */

/**
 * Menu callback.
 * Generates Profile Tools default category tab settings page.
 */
function profile_tools_default_tab_settings() {
  $categories = _profile_tools_categories();
  if (sizeof($categories) > 1) {
    $output = drupal_get_form('profile_tools_default_tab_form', $categories);
  }
  else {
    $output = t('There is only one available user profile category currently, nothing to set as default tab.');
  }
  return $output;
}

/**
 * Define a profile_tools_default_tab_form.
 *
 * @param array $form_state
 * @param array $categories
 *
 * @return array $form
 *
 * @ingroup form
 */
function profile_tools_default_tab_form(&$form_state, $categories) {
  $form = array();
  
  $form['category'] = array(
    '#type' => 'select',
    '#title' => t('Select default category'),
    '#default_value' => variable_get('profile_tools_default_tab', 'account'),
    '#options' => $categories,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

/**
 * Handle profile_tools_default_tab_form submission.
 */
function profile_tools_default_tab_form_submit($form, &$form_state) {
  $category = $form_state['values']['category'];
  variable_set('profile_tools_default_tab', $category);
  menu_rebuild();
  drupal_set_message(t('Default category tab have been saved.'));
}

/**
 * Adds additional elements to user picture settings form.
 *
 * @ingroup form
 */
function profile_tools_form_user_admin_form(&$form, $form_state) {
  $categories = _profile_tools_categories();
  $form['pictures']['settings']['profile_tools_user_picture_category'] = array(
    '#type' => 'select',
    '#title' => t('User picture fieldset placement tab'),
    '#default_value' => variable_get('profile_tools_user_picture_category', 'account'),
    '#options' => $categories,
    '#description' => t('User picture fieldset will be moved to selected profile category.'),
  );
  $form['pictures']['settings']['profile_tools_user_picture_weight'] = array(
    '#title' => t('User picture fieldset weight'),
    '#type' => 'weight',
    '#default_value' => variable_get('profile_tools_user_picture_weight', -100),
    '#delta' => 100,
    '#description' => t('User picture fieldset form weight.'),
  );
}
